"""myges URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('app.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),

    # path('promotion/', views.list_promotion, name='promo_list'),
    # path('promotion/details/<id>', views.detail_promotion, name='detail_promo'),
    # path('promotion/update/<id>', views.update_promotion, name='update_promo'),
    # path('promotion/delete/<id>', views.delete_promotion, name='delete_promo'),
    #
    # path('subject/', views.list_subject, name='subject_list'),
    # path('subject/details/<id>', views.detail_subject, name='detail_subject'),
    # path('subject/update/<id>', views.update_subject, name='update_subject'),
    # path('subject/delete/<id>', views.delete_subject, name='delete_subject'),
    #
    # path('user/new', views.add_user, name='add_user'),
    # path('user/', views.list_user, name='user_list'),
    # path('user/details/<id>', views.detail_user, name='detail_user'),
    # path('user/update/<id>', views.update_user, name='update_user'),
    # path('user/delete/<id>', views.delete_user, name='delete_user'),
]
