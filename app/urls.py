from django.urls import path

from . import views

urlpatterns = [
    path('promotion/', views.list_promotion, name='promo_list'),
    path('promotion/details/<id>', views.detail_promotion, name='detail_promo'),
    path('promotion/update/<id>', views.update_promotion, name='update_promo'),
    path('promotion/delete/<id>', views.delete_promotion, name='delete_promo'),
    path('promotion/new', views.add_promotion, name='add_promo'),

    path('subject/new', views.add_subject, name='add_subject'),
    path('subject/', views.list_subject, name='subject_list'),
    path('subject/details/<id>', views.detail_subject, name='detail_subject'),
    path('subject/update/<id>', views.update_subject, name='update_subject'),
    path('subject/delete/<id>', views.delete_subject, name='delete_subject'),

    path('user/new', views.add_user, name='add_user'),
    path('user/', views.list_user, name='user_list'),
    path('user/details/<id>', views.detail_user, name='detail_user'),
    path('user/update/<id>', views.update_user, name='update_user'),
    path('user/delete/<id>', views.delete_user, name='delete_user'),

    path('user_promo_list/', views.list_user_from_promotion, name='user_promo_list'),
    path('teacher-dashboard', views.teacher_dashboard, name='teacher_dashboard'),

]
