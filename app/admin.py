from django.contrib import admin

from .models import Promotion, Subject

admin.site.register(Promotion)
admin.site.register(Subject)
