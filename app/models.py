from django.db import models


class Subject(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200).null
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)


class Promotion(models.Model):
    name = models.CharField(max_length=200)
    user = models.ManyToManyField('auth.user')
    subject = models.ManyToManyField(Subject)
