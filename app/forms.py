from django import forms
from .models import Promotion, Subject
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError
from pprint import pprint

ROLE_CHOICES = [
    ('2', 'Student'),
    ('1', 'Teachers'),
    ('3', 'Coordinators'),
]


class CustomSubject(forms.ModelMultipleChoiceField):
    def label_from_instance(self, subject):
        return "%s" % subject.name


class CustomUser(forms.ModelMultipleChoiceField):
    def label_from_instance(self, user):
        return "%s" % user.username


class Add_Promotion(forms.ModelForm):

    # create meta class
    class Meta:
        # specify model to be used
        model = Promotion

        # specify fields to be used
        fields = [
            "name", "user", "subject"
        ]
    user = CustomUser(
        queryset=User.objects.filter(groups=2),
        widget=forms.CheckboxSelectMultiple
    )

    subject = CustomSubject(
        queryset=Subject.objects.all(),
        widget=forms.CheckboxSelectMultiple
    )


class Add_Subject(forms.ModelForm):
    class Meta:
        model = Subject
        fields = [
            "name",
            "user"
        ]

    #user = forms.Select(choices=User.objects.filter(groups=1))

    user = forms.ModelChoiceField(
        queryset=User.objects.filter(groups=1)
    )


class CustomUserCreationForm(forms.Form):
    username = forms.CharField(
        label='Enter Username', min_length=4, max_length=150)
    firstname = forms.CharField(
        label='Enter firstname', min_length=4, max_length=150)
    lastname = forms.CharField(
        label='Enter lastname', min_length=4, max_length=150)
    email = forms.EmailField(label='Enter email')
    groups = forms.ChoiceField(choices=ROLE_CHOICES)
    password1 = forms.CharField(
        label='Enter password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Confirm password', widget=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    def clean_firstname(self):
        firstname = self.cleaned_data['firstname'].lower()
        return firstname

    def clean_lastname(self):
        lastname = self.cleaned_data['lastname'].lower()
        return lastname

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2

    def save(self, commit=True):
        group = Group.objects.get(id=self.cleaned_data['groups'])
        pprint(group)
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1'],
            first_name=self.cleaned_data['firstname'],
            last_name=self.cleaned_data['lastname'],
            # groups=self.cleaned_data['group'],

        )
        user.groups.add(group.id)
        return user
